## counter-service 
Counts the number of post requests arrived at counter-service and returns it as a response upon counter-service get request

- use `curl -s -X POST http://ec2-18-194-117-172.eu-central-1.compute.amazonaws.com/counter-service` to increase the count

- use `curl -s http://ec2-18-194-117-172.eu-central-1.compute.amazonaws.com/counter-service` to receive the counter