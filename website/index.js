// use express js
const express = require('express')
const morgan = require('morgan');

const app = express()
const port = 80

var countPostReqs = 0  // the counter

// log tiny messages
app.use(morgan('tiny'));

// upon get request - reply with the current counter (of post requests)
app.get("/counter-service", (req, res) => {
    res.send(`${countPostReqs}`);
});

// upon post request - increment the counter
app.post("/counter-service", (req, res) => {
    countPostReqs++;
    res.end();
});

app.listen(port, () => 
    console.log(`Example app listening on port ${port} - use /counter-service for actual counter!`)
);
